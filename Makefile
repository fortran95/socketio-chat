all: client_src/head.html client_src/middle.html client_src/foot.html concat.js.tmp concat.css.tmp body.tmp
	cat client_src/head.html concat.js.tmp concat.css.tmp client_src/middle.html body.tmp client_src/foot.html > index.html

clean:
	rm *.tmp

concat.js.tmp: client_src/js/*.js
	echo "<script>(function(){" > concat.js.tmp

	cat client_src/js/*.js > concat.uncompressed.js
	uglifyjs concat.uncompressed.js >> concat.js.tmp
	rm concat.uncompressed.js

	echo "})();</script>" >> concat.js.tmp

concat.css.tmp: client_src/css/*.css
	echo "<style>" > concat.css.tmp

	cat client_src/css/*.css > concat.uncompressed.css
	java -jar yuicompressor.jar concat.uncompressed.css >> concat.css.tmp
	rm concat.uncompressed.css

	echo "</style>" >> concat.css.tmp

body.tmp: client_src/body.html
	cat client_src/body.html > body.tmp
