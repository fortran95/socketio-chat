var app = require('http').createServer(handler)
var io = require('socket.io')(app);
var fs = require('fs');
var crypto = require('./_crypto.js');

var localStorageBuilder = require('node-localstorage').LocalStorage;


app.listen(8200);

function handler (req, res) {
  fs.readFile(__dirname + '/index.html',
  function (err, data) {
    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }

    res.writeHead(200);
    res.end(data);
  });
}

//////////////////////////////////////////////////////////////////////////////

var room = new (function(){
    var self = this;
    var storage = new localStorageBuilder('storage-room');

    var sessionMixKeyBuf = new crypto.util.srand().bytes(32);

    storage.setItem('testtestte.uuid', 'a9b7547a-8fb9-11e4-99ee-3c970e55a4ces');
    storage.setItem('testtestte.auth', 'testpassword');

    this.exists = function(name){
        return (null != storage.getItem(name + '.uuid'))
    };

    var cacheChannelID = {};
    this.getChannelID = function(name){
        if(cacheChannelID[name]) return cacheChannelID[name];
        var record = storage.getItem(name + '.uuid');
        if(!record) return null;
        var hasher = new crypto.hash('RIPEMD160');

        var result = hasher.mac(
            crypto.util.encoding(record, 'ascii').toArrayBuffer(),
            sessionMixKeyBuf
        ).hex;
        cacheChannelID[name] = result;
        return result;
    };

    var userList = {};

    this.inRoom = function(roomName, nickName){
        var record = storage.getItem(roomName + '.uuid');
        if(!record) return false;
        if(undefined === userList[roomName]){
            userList[roomName] = {};
            return false;
        };
        return Boolean(undefined !== userList[roomName][nickName]);
    };

    this.addToRoom = function(roomName, nickName){
        // join nickName to given roomName
        if(self.inRoom(roomName, nickName)) return false;
        userList[roomName][nickName] = {};
        self.broadcastRoomInfo(roomName);
        return true;
    };

    this.recordInRoom = function(roomName, nickName, key, value){
        if(!self.inRoom(roomName, nickName)) return false;
        userList[roomName][nickName][key] = value;
        self.broadcastRoomInfo(roomName);
    };

    this.showRoom = function(roomName){
        if(undefined === userList[roomName]) return false;
        var ret = {};
        for(var nickName in userList[roomName])
            ret[nickName] = userList[roomName][nickName];
        console.log(ret);
        return ret;
    };

    this.removeFromRoom = function(roomName, nickName){
        if(!self.inRoom(roomName, nickName)) return;
        delete userList[roomName][nickName];
        self.broadcastRoomInfo(roomName);
    };

    this.broadcastRoomInfo = function(roomName){
        var channelID = self.getChannelID(roomName);
        if(!channelID) return;
        io.to(channelID).emit('event', { roominfo: self.showRoom(roomName) });
        // this is the `PUSH`. another `PULL` is also implemented. see below.
    };

    return this;
})();

//////////////////////////////////////////////////////////////////////////////

io.on('connection', onNewConnection);
function onNewConnection(socket){
    var session = {
        nickname: null,
        room: null,
        channelID: null,
    };

    socket.emit('event', { connected: true });

    socket.on('disconnect', function(){
        if(session.nickname && session.room){
            io.to(session.channelID).emit('event', {
                leave: session.nickname,
            });
            room.removeFromRoom(session.room, session.nickname);
        };
        session.nickname = null;
        session.room = null;
        session.channelID = null;
    });
   

    // handles settings

    socket.on('set.nickname', function(requestedNickname){
        // connection identifies its nickname
        if(
            'string' == typeof requestedNickname &&
            requestedNickname.length < 30 &&
            requestedNickname.length >= 4
        ){
            session.nickname = requestedNickname;
            return socket.emit('event', { nickname: session.nickname });
        };
        return socket.emit('event', { nickname: false });
    });

    socket.on('set.room', function(requestedRoom){
        // connection sets or changes its room
        
        if(!/^[0-9a-z]{10}$/i.test(requestedRoom))
            return socket.emit('event', { room: false });
        requestedRoom = requestedRoom.toLowerCase();

        if(!room.exists(requestedRoom))
            return socket.emit('event', {
                room: false,
                other: 'nonexistent',
            });

        // test if room manager allows user with this name to join
        var joinResult = room.addToRoom(requestedRoom, session.nickname);
        var channelID = room.getChannelID(requestedRoom);
        if(false !== joinResult){
            // allowed join
            if(channelID){
                socket.join(channelID);
                io.to(channelID).emit('event', {
                    joined: session.nickname,
                });
                session.room = requestedRoom;
                session.channelID = channelID;
                return socket.emit('event', { room: session.room });
            };
        } else {
            if(channelID){
                io.to(channelID).emit('event', {
                    "try-joining": session.nickname,
                });
            };
            return socket.emit('event', {
                room: false,
                other: 'occupied',
            });
        };

        return socket.emit('event', {
            room: false,
            other: 'unknown',
        });
    });

    socket.on('set.pubkey', function(pubkey){
        // connection updates its public key
        if(
            'string' == typeof pubkey &&
            pubkey.length <= 768 &&
            /^[0-9a-z\+\/=].+$/i.test(pubkey)
        ){
            room.recordInRoom(
                session.room,
                session.nickname,
                'pubkey',
                pubkey
            );
            return socket.emit('event', { pubkey: true });
        };
        return socket.emit('event', { pubkey: false });
    });


    // list room
    socket.on('get.roominfo', function(){
        if(!session.room) return;
        return socket.emit('event', { roominfo: room.showRoom(session.room) });
        // this is the `PULL`. another `PUSH` is also implemented. see above.
    });


    socket.on('message', function(v){
        if(session.nickname && session.room){
            var forwarded = {};

            forwarded.sender = session.nickname;
            forwarded.authenticated = false;
            forwarded.data = v.data;
            forwarded.encrypted = Boolean(v.encrypted);

            socket.emit('event', { message: true });
            return io.to(session.channelID).emit('message', forwarded);
        };
        socket.emit('event', { message: false });
    });


};
