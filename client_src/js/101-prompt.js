var PROMPT = new (function(){
//////////////////////////////////////////////////////////////////////////////
var self = this;

var handler = null;

self.setPrompt = function(v){
    $('#prompt').text(v);
    return self;
};

self.setHandler = function(func){
    handler = func;
    return self;
};

self.setShadow = function(bool){
    if(true === bool)
        $('#input').attr('type', 'password');
    else
        $('#input').attr('type', 'text');
};

self.clear = function(){
    $('#input').val('');
    self.setShadow(false);
};

self.enter = function(value){
    // emulate a user input
    enterPressed(value);
};

function enterPressed(value){
    if(!value) value = $('#input').val();
    if(handler) handler(value, self);
};

$(function(){
    $('#input').keypress(function(e){
        if(13 != e.keyCode) return;
        enterPressed();
    });
});

return this;
//////////////////////////////////////////////////////////////////////////////
})();
