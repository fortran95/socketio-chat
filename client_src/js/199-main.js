/*
 * TODO
 *
 * 会话公钥如果能和用户昵称绑定并存储在localStorage，则可大大方便区分用户！
 * 可努力实现这一点。
 */

$(function(){
//////////////////////////////////////////////////////////////////////////////

var localIdentity = CRYPTO.enigma.identity();
localIdentity.generate('Dummy Key for This User', {algorithm: 'NECRAC112'});
OUTPUT.success('会话公钥生成, 算法套装 ' + localIdentity.getAlgorithm());

var localIdentityPubkeyB64 = 
    CRYPTO.util.encoding(localIdentity.exportPublic()).toBase64();


function socketWorker(){
    var self = this;

    var socket = io(__CONFIG.src, {path: __CONFIG.srcPath});
    socket.on('event', function (data) {
        if(undefined !== data.connected) return loop.start();

        if(undefined !== data.nickname){
            self.emit('feedback.set.nickname', data.nickname);
            return;
        };

        if(undefined !== data.room){
            self.emit('feedback.set.room', data.room, data.other);
            return;
        };

        if(undefined !== data.pubkey){
            self.emit('feedback.set.pubkey', Boolean(data.pubkey));
            return;
        };

        if(undefined != data.roominfo){
            self.emit('room.update', data.roominfo);
            return;
        };

        if(undefined != data.joined){
            self.emit('room.joined', data.joined, data.payload);
            return;
        };

        if(undefined != data["try-joining"]){
            self.emit('room.try-joining', data["try-joining"]);
            return;
        };

        if(undefined != data.leave){
            self.emit('room.leave', data.leave);
            return;
        };

        if(undefined != data.message){
            self.emit('message.sent', data.message);
            return;
        };
    });

    socket.on('message', function(v){
        self.emit('message.received', v);
    });

    this.socket = socket;
    return this;
};
heir.inherit(socketWorker, EventEmitter);
var worker = new socketWorker();

/****************************************************************************/

var room = new (function(){
    var self = this;

    var info = {};

    this.update = function(roominfo){
        var removeList = [];
        for(var i in info) if(undefined === roominfo[i]) removeList.push(i);
        for(var i in removeList) delete info[removeList[i]];

        for(var nickname in roominfo){
            if(undefined === info[nickname]) info[nickname] = {};
            if(roominfo[nickname].pubkey != info[nickname].pubkey){
                var identityPubkeyB64 = roominfo[nickname].pubkey
                info[nickname].pubkey = identityPubkeyB64;
                try{
                    var identityPubkeyBuf = CRYPTO.util.encoding(
                        identityPubkeyB64, 
                        'base64'
                    ).toArrayBuffer();
                    var identity = CRYPTO.enigma.identity();
                    identity.loadPublic(identityPubkeyBuf);
                    info[nickname].identity = identity;
                } catch(e){
                    console.error(e)
                    info[nickname].identity = null;
                };
            };
        };
    };

    this.encryptBroadcastB64 = function(text){
        var message = CRYPTO.enigma.message();
        var textBuf = CRYPTO.util.encoding(text).toArrayBuffer();
        message.write(textBuf);
        try{
            for(var nickname in info){
                if(!info[nickname].identity) continue;
                message.encrypt(info[nickname].identity);
            };
            message.encrypt(localIdentity);
            var encryptedBuf = message.done();
        } catch(e){
            return false;
        };
        return CRYPTO.util.encoding(encryptedBuf).toBase64();
    };

    this.tryDecryptBroadcast = function(b64){
        try{
            var buf = CRYPTO.util.encoding(b64, 'base64').toArrayBuffer();
            var message = CRYPTO.enigma.message();
            message.read(buf);
            if(undefined != message.decrypt) message.decrypt(localIdentity);
            if(undefined != message.getPlaintext){
                var plaintext = message.getPlaintext();
                return CRYPTO.util.encoding(plaintext).toUTF16();
            };
        } catch(e){
            console.log(e)
        };
        return false;
    };

    return this;
})();

/****************************************************************************/

var loop = new (function Loop(){
    var self = this;

    var session = {
        nickname: null,
        room: __CONFIG.defaultRoom,
        first: true,
    };

    self.start = function(){
        if(session.first){
            OUTPUT.log('确认服务器连接, 聊天程序启动.');
            session.first = false;
        } else
            OUTPUT.log('重新连接服务器, 正在重新加入.');
        _startLoginSequence();
    };

    function _startLoginSequence(){
        setNickname();
    };

    function setNickname(){
        function doer(tryNickname, p){
            worker.once('feedback.set.nickname', function(v){
                if(false == v){
                    session.nickname = null;
                    OUTPUT.error('昵称无效, 重新输入.');
                    return;
                };
                session.nickname = v;
                OUTPUT.success('昵称成功设定为: ' + session.nickname);
                p.clear();
                joinRoom(); // next step
            });
            OUTPUT.log('尝试设定昵称: ' + tryNickname);
            worker.socket.emit('set.nickname', tryNickname);
        };
        PROMPT.setPrompt('请自拟昵称').setHandler(doer);
        if(session.nickname) PROMPT.enter(session.nickname);
    };

    function joinRoom(){
        function doer(tryRoom, p){
            if(!/^[0-9a-z]{10}$/i.test(tryRoom)){
                OUTPUT.error('聊天室ID无效, 重新输入.');
                return;
            };
            worker.once('feedback.set.room', function(v, other){
                if(false == v){
                    var reason = {
                        'nonexistent': '聊天室不存在',
                        'occupied': '禁止加入, 可能成员已存在',

                    }[other] || '未知错误.';
                    OUTPUT.error('加入聊天室失败. 理由: ' + reason);
                    session.room = null;
                    return;
                };
                session.room = tryRoom;
                OUTPUT.success('聊天室成功设定为: ' + v);
                p.clear();
                publishPubkey(); // next step
            });
            OUTPUT.log('尝试设定聊天室为: ' + tryRoom);
            worker.socket.emit('set.room', tryRoom);
        };
        PROMPT.setPrompt('请输入聊天室ID').setHandler(doer);
        if(session.room) PROMPT.enter(session.room);
    };

    function publishPubkey(){
        worker.once('feedback.set.pubkey', function(v){
            startChat(); // next step
            if(false == v){
                OUTPUT.error('服务器拒绝接收会话公钥, 别人将无法向您加密发送消息.');
                return;
            };
            OUTPUT.success('会话公钥成功发布');
        });
        OUTPUT.log('将会话公钥通过服务器发布');
        worker.socket.emit('set.pubkey', localIdentityPubkeyB64);
    };
   
   
    // accept user input as messages

    function startChat(){
        var commands = {};

        commands["help"] = function(args, p){
            var list = Object.keys(commands);
            OUTPUT.log('::: 当前可用的命令有: ' + list.join(' '));
            modeChat();
        };

        commands["auth"] = function(args, p){
        };
        

        function modeChatHandler(userInput, p){
            // if this is a command
            var cmdprefix = '';
            for(var cmdname in commands){
                cmdprefix = '|' + cmdname;
                if(
                    userInput.slice(0, cmdprefix.length).toLowerCase() == 
                    cmdprefix
                ){
                    commands[cmdname](userInput.trim().split(' ').slice(1), p);
                    return;
                };
            };

            // or send it
            var encryptedB64 = room.encryptBroadcastB64(userInput);
            worker.once('message.sent', function(){
                p.clear();
            });
            worker.socket.emit('message', {
                data: encryptedB64,
                encrypted: true,
            });
        };
        function modeChat(){
            PROMPT
                .setPrompt(
                    '您的昵称 ' + session.nickname + ' ' +
                    '当前聊天室 ' + session.room + ' 输入后按回车发送. ' +
                    '查看特殊命令帮助请输入 |help (注意连字符)'
                )
                .setHandler(modeChatHandler)
            ;
            worker.socket.emit('get.roominfo');
        };
        modeChat(); // accepts user input as message
    };

    // other messages

    worker.on('room.joined', function(name, payload){
        OUTPUT.warning('网友 ' + name + ' 加入了聊天室');
    });

    worker.on('room.try-joining', function(name){
        OUTPUT.warning('试图使用 ' + name + ' 为名加入聊天室的请求被拒绝');
    });

    worker.on('room.leave', function(name){
        OUTPUT.warning('网友 ' + name + ' 已经下线');
    });

    worker.on('room.update', function(info){
        if(!info) return;
        room.update(info);
        OUTPUT.log(
            '更新在线列表. 当前在线 ' +
            Object.keys(info).length + 
            ' 人.'
        );
    });


    // new message

    worker.on('message.received', function(msg){
        var sender = msg.sender;
        if(msg.encrypted){
            var decrypted = room.tryDecryptBroadcast(msg.data);
            if(false === decrypted)
                return OUTPUT.error('收到一条无法解密的消息');
            OUTPUT.message('<' + sender + '> 说: ' + decrypted);
            return;
        };
        OUTPUT.log(JSON.stringify(msg));        
    });

    return this;
})();;

////////////////////////////////////////////////////////////////////////////// 
});
