var OUTPUT = new (function(){
//////////////////////////////////////////////////////////////////////////////
var self = this;

function nowtime(){
    return new Date().toLocaleString();
};

function displayNewMessage(v){
    return $('<div>')
        .text('[' + nowtime() + '] ' + v)
        .prependTo('#history')
    ;
};

self.log = function(v){
    displayNewMessage(v).addClass('log');
};

self.error = function(v){
    displayNewMessage(v).addClass('error');
};

self.success = function(v){
    displayNewMessage(v).addClass('success');
};

self.warning = function(v){
    displayNewMessage(v).addClass('warning');
};

self.message = function(sender, v){
    displayNewMessage(sender)
        .append($('<div>').text(v))
        .addClass('message')
    ;
};

return this;
//////////////////////////////////////////////////////////////////////////////
})();
