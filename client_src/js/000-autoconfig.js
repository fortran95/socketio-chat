var __CONFIG = (function(){
    var config = {};

    // config socket io url
    if('localhost' == window.location.hostname && false){
        config.src = 'http://localhost:8200';
        config.srcPath = '/';
    } else {
        if('https' == window.location.href.slice(0,5)){
            config.src = "https://neoatlantis.info:443/";
        } else {
            config.src = "http://neoatlantis.info:80/";
        };
        config.srcPath = '/socketio-chat';
    };

    // read defaults passed from the URL hashtag
    if(window.location.hash)
        config.defaultRoom = window.location.hash.slice(1);
    else
        config.defaultRoom = null;

    return config;
})();
